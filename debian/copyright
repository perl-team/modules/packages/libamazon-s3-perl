Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Source: https://metacpan.org/release/Amazon-S3
Upstream-Contact: Timothy Appnel <tima@cpan.org>
Upstream-Name: Amazon-S3

Files: *
Copyright: 2008-2009, Timothy Appnel <tima@cpan.org>
 2006-2007, Brad Fitzpatrick <brad@danga.com>
 2006-2007, Leon Brocard <acme@astray.com>
 2006, Amazon Digital Services, Inc. or its affiliates
License: Artistic
Comment: Amazon-S3 was forked from Net::Amazon::S3.
 Net::Amazon::S3 was based on example code from Amazon with this notice:
 .
 This software code is made available "AS IS" without warranties of any
 kind. You may copy, display, modify and redistribute the software
 code either by itself or as incorporated into your code; provided that
 you do not remove any proprietary notices. Your use of this software
 code is at your own risk and you waive any claim against Amazon
 Digital Services, Inc. or its affiliates with respect to your use of
 this software code. (c) 2006 Amazon Digital Services, Inc. or its
 affiliates.

Files: debian/*
Copyright: 2016-2018, Christopher Hoskin <mans0954@debian.org>
License: Artistic or GPL-1+

License: Artistic
 This program is free software; you can redistribute it and/or modify
 it under the terms of the Artistic License, which comes with Perl.
 .
 On Debian systems, the complete text of the Artistic License can be
 found in `/usr/share/common-licenses/Artistic'.

License: GPL-1+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 1, or (at your option)
 any later version.
 .
 On Debian systems, the complete text of version 1 of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-1'.
